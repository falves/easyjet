#include "../BaselineVarsXbbCalibAlg.h"
#include "../BaselineVarsZbbyCalibAlg.h"
#include "../ZbbyCalibSelectorAlg.h"
#include "../ZcandSelectorAlg.h"
#include "../BaselineVarsZbbjCalibAlg.h"
#include "../BaselineVarsZllCalibAlg.h"
#include "../XbbCalibSelectorAlg.h"
#include "../ZbbjCalibSelectorAlg.h"
#include "../ZllCalibSelectorAlg.h"

using namespace XBBCALIB;

DECLARE_COMPONENT(BaselineVarsXbbCalibAlg)
DECLARE_COMPONENT(BaselineVarsZbbyCalibAlg)
DECLARE_COMPONENT(ZbbyCalibSelectorAlg)
DECLARE_COMPONENT(ZcandSelectorAlg)
DECLARE_COMPONENT(BaselineVarsZbbjCalibAlg)
DECLARE_COMPONENT(BaselineVarsZllCalibAlg)
DECLARE_COMPONENT(XbbCalibSelectorAlg)
DECLARE_COMPONENT(ZbbjCalibSelectorAlg)
DECLARE_COMPONENT(ZllCalibSelectorAlg)
